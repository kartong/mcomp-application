# Overview
This is a repository depicting a small subset of the codes that I have written over the years while working as a bioinformatician at the Cancer Science Institute of Singapore.

## Python

I've gradually shifted some of my work from Perl to Python as I have found it easier to write reusable code with it than I had experienced with Perl.

###samReadClass.py
A python class to depict and parse a SAM read object. The SAM format is a standard format that people in the next-generation sequencing field utilize to represent information regarding a 'read' representing a sequence generated from one of the next-generation sequencing machine.

This class makes it easy for me to extract and parse information that I need for other downstream applications.


###get1000G_editingsites.py
This demonstrates the usage of the multiprocessing module in Python for running multiple concurrent processes simultaneously. Here, I also utilized a ramdisk to speed up random access and processing of multiple positions in a single input file in order to prevent an IO choke, especially since 10-20 processes will be doing the random access on that single file simultaneously.



## Perl
###annotateRNA/annotateRNAFeaturesPipe.pl
A Perl based program to annotate RNA features based on genomics coordiantes


###Aligner/novoalignParallelClipGeneral.pl
A small hacked version of a commercial next-generation sequencing aligner which I use to enable multi-threading of the original software.

The software is free to use for single threaded process but the multi-threading function is unfortunately only available if I pay for it. Thus, instead of relying on the multi-threadings function in the original software, I wrote a wrapper around the original software and did the data splitting and parallelization myself using the Perl Parallel::ForkManager module.