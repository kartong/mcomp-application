#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

# This script calls for the strand of the annotation and the
# final nucleotide conversion based on the strand




my $file = $ARGV[0];



my $refNtCol = 4;
my $varNtCol = 5;
my $transcriptAnnoCol = 9;


GetOptions(
        'refNTCol:i' => \$refNtCol,
        'varNTCol:i' => \$varNtCol,
	'transcriptAnnoCol' => \$transcriptAnnoCol
);

if(@ARGV != 1){
	print "Usage: $0 <file>\n";
	print "============================================================\n";
	print "Note that column number is indicated as zero-based\n";
	print " -refNTcol [int]            : Column with the ref nucleotide (default=$refNtCol)\n";
	print " -varNTcol [int]            : Column with the var nucleotide (default=$varNtCol)\n";
	print " -transcriptAnnoCol [int]   : Column with the transcript annotation (default=$transcriptAnnoCol)\n";
	exit(0);
}




open(INPUT, $file);

while(my $line = <INPUT>){
	$line =~ s/\r\n//;
	chomp($line);
	my @line_array = split(/\t/, $line);


	my $refNt = $line_array[$refNtCol];
	my $varNt = $line_array[$varNtCol];
	
	# Make the call for the strand of the line;
	my $transcript_anno =  $line_array[$transcriptAnnoCol];
	my $strand_call;	
	unless($transcript_anno eq "NA"){
		my @transcript_anno_array = split(/\|/, $transcript_anno);
		
		my $plus_hit  = 0;
		my $minus_hit = 0;
		
		foreach(@transcript_anno_array){
			my @anno_split = split(/\;/, $_);
			my $strand = $anno_split[2];
			
			if($strand eq "+"){
				$plus_hit = 1;
			}
			elsif($strand eq "-"){
				$minus_hit = 1;
			}
		}
		
		# Process the plus and minus hit tag
		if($plus_hit && $minus_hit){
			$strand_call = "both";
		}
		elsif($plus_hit && not($minus_hit)){
			$strand_call = "plus";
		}
		elsif(not($plus_hit) && $minus_hit){
			$strand_call = "minus";
		}
		# Should not have this case. Give error
		else{
			$strand_call = "Error";
		}
	}
	else{
		$strand_call = "NA";
	}
	
	# Make the nucleotide change call
	my $conversionCall = $refNt . ">" . $varNt;
	$conversionCall =~ tr/atgcn/ATGCN/;
	
	
	
	# Define the final common editing event
	my @final_differential_editing_call;
	if($strand_call eq "plus"){
	}
	elsif($strand_call eq "minus"){
		$conversionCall =~ tr/ATCG/TAGC/;
	}
	elsif($strand_call eq "both"){
		$conversionCall = "ambiguous";
	}
        else{
                $conversionCall = "NA";
        }
	
	# Print out the output
	print $line . "\t" . $strand_call . "\t" . $conversionCall . "\n";
}

close(INPUT);


