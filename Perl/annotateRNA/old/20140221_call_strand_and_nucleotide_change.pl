#!/usr/bin/perl

use strict;
use warnings;

# This script calls for the strand of the annotation and then
# 




my $file = $ARGV[0];

my $pval_cutoff = 4;
my $delta_vaf_cutoff = 0.1;



open(INPUT, $file);

while(my $line = <INPUT>){
	$line =~ s/\r\n//;
	chomp($line);
	my @line_array = split(/\t/, $line);
	
	# Make the call for the strand of the line;
	my $transcript_anno =  $line_array[226];	
	my $strand_call;	
	unless($transcript_anno eq "NA"){
		my @transcript_anno_array = split(/\|/, $transcript_anno);
		
		my $plus_hit  = 0;
		my $minus_hit = 0;
		
		foreach(@transcript_anno_array){
			my @anno_split = split(/\;/, $_);
			my $strand = $anno_split[2];
			
			if($strand eq "+"){
				$plus_hit = 1;
			}
			elsif($strand eq "-"){
				$minus_hit = 1;
			}
		}
		
		# Process the plus and minus hit tag
		if($plus_hit && $minus_hit){
			$strand_call = "both";
		}
		elsif($plus_hit && not($minus_hit)){
			$strand_call = "plus";
		}
		elsif(not($plus_hit) && $minus_hit){
			$strand_call = "minus";
		}
		# Should not have this case. Give error
		else{
			$strand_call = "Error";
		}
	}
	else{
		$strand_call = "NA";
	}
	
	# Make the nucleotide change call
	
	
	# 189 - 202
	# 203 - 216
	
	my @hit_num_increase;
	my @hit_num_decrease;
	my @hit_num;
	my $hit_flag = 0;
	
	for(my $i=1; $i<=14; $i++){
		my $p_col		= 188 + $i;
		my $delta_col	= 202 + $i;
		
		my $pval 		= $line_array[$p_col];
		my $delta_val 	= $line_array[$delta_col];
		
		if($pval >= $pval_cutoff && $delta_val >= $delta_vaf_cutoff){
			push(@hit_num_increase, $i);
			push(@hit_num, $i);
			$hit_flag = 1;
		}
		elsif($pval >= $pval_cutoff && $delta_val <= - $delta_vaf_cutoff){
			push(@hit_num_decrease, $i);
			push(@hit_num, $i);
			$hit_flag = 1;
		}
	}
	
	# Make call for the max increase and decrease nt
	my @differential_editing_nt;
	my %differential_editing_hash;
	foreach my $i (@hit_num){
		my $norm_col = 3 + ($i - 1) * 12;
		my $tum_col	 = 9 + ($i - 1) * 12;
		
		my @norm_count = ($line_array[$norm_col], $line_array[$norm_col + 1], 
						  $line_array[$norm_col + 2], $line_array[$norm_col + 3]);
		
		my @tum_count  = ($line_array[$tum_col], $line_array[$tum_col + 1], 
						 $line_array[$tum_col + 2], $line_array[$tum_col + 3]);
	
		my ($max_inc_nt, $max_dec_nt) = call_maximal_differential_nt(\@norm_count, \@tum_count);
		my $nt_call = $max_dec_nt . "_" . $max_inc_nt;
		
		push(@differential_editing_nt, $nt_call);
		$differential_editing_hash{$nt_call} += 1;
		
	}
	
	# Define the final common editing event
	my @final_differential_editing_call;
	if($strand_call eq "plus"){
		foreach( keys(%differential_editing_hash) ){
			push(@final_differential_editing_call, $_);
		}
	}
	elsif($strand_call eq "minus"){
		foreach( keys(%differential_editing_hash) ){
			my $nt_call = $_;
			$nt_call =~ tr/ATCG/TAGC/;
			push(@final_differential_editing_call, $nt_call);
		}
	}
	elsif($strand_call eq "both"){
		push(@final_differential_editing_call, "ambiguous");
	}
	else{
		push(@final_differential_editing_call, "NA");
	}
	
	# Make sure that all the empty arrays have a space in them
	# if( undef(@hit_num_increase) ){ push(@hit_num_increase, " ") }
	# if( undef(@hit_num_decrease) ){ push(@hit_num_decrease, " ") }
	# if( undef(@hit_num) ){ push(@hit_num, " ") }
	# if( undef(@differential_editing_nt) ){ push(@differential_editing_nt, " ") }
	#if( undef(@final_differential_editing_call) ){ push(@final_differential_editing_call, " ") }
	
	
	# Print out the output
	if($hit_flag){
		print $line . "\t" . $strand_call . "\t" . join(",", @hit_num_increase) . "\t" .
		join(",", @hit_num_decrease) . "\t" . join(",", @hit_num) . "\t" . join("|",@differential_editing_nt) .
		"\t" . join("|", @final_differential_editing_call) . "\t" . scalar(@hit_num ). "\n";
	}
}

close(INPUT);




sub call_maximal_differential_nt{
	my @normal_count	= @{ $_[0] };
	my @tumor_count		= @{ $_[1] };

	# Get total nucleotide count
	my $normal_total;
	my $tumor_total;
	
	foreach(@normal_count){
		$normal_total += $_;
	}
	foreach(@tumor_count){
		$tumor_total += $_;
	}
	
	# Get proportion of each nt
	my @normal_prop;
	my @tumor_prop;
	
	foreach(@normal_count){
		push(@normal_prop,  $_ / $normal_total);
	}
	foreach(@tumor_count){
		push(@tumor_prop,  $_ / $normal_total);
	}
	
	# Calculate the difference in proportion
	my %prop_difference_all;
	my @nt_array = qw(A C G T);
	
	
	for(my $i=0; $i<scalar(@normal_prop); $i++){
		my $prop_difference = $tumor_prop[$i] - $normal_prop[$i];
		my $nt = $nt_array[$i];
		
		$prop_difference_all{$nt} = $prop_difference;
	}
	
	my @nt_order = sort{ $prop_difference_all{$a} <=> $prop_difference_all{$b} } keys(%prop_difference_all);
	
	my $max_increase_nt = $nt_order[0];
	my $max_decrease_nt = $nt_order[3];
	
	return($max_increase_nt, $max_decrease_nt);
}







