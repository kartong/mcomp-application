#!/usr/bin/perl

use strict;
use warnings;
use Parallel::ForkManager;

use Getopt::Long;
use File::Basename;


# This script will annotate a particular position as belonging to which region of a gene

# Sample command: ./20131216_mutation_annotation_pipe.pl /home/Kar\ Tong/mutation_analysis/mutation_catalogue/ftp.sanger.ac.uk/pub/cancer/AlexandrovEtAl/somatic_mutation_data/ALL/ALL_clean_somatic_mutations_for_signature_analysis.txt


my $prog = basename ($0);

# Information Needed for analysis
# column num is zero-based
my $chr_col			= 0;
my $chr_format_full	= 1; # 0 if it is "1,2,3," format and 1 if "chr1,chr2,chr3," format
my $start_posn_col	= 1;
my $end_posn_col	= 1;
my $mut_type		= 0; # 1 if mutation type is annotated (i.e. SNP/indel)
my $mut_type_col	= 1;

#my $somatic_mutation_file = '/home/Kar Tong/mutation_analysis/mutation_catalogue/ftp.sanger.ac.uk/pub/cancer/AlexandrovEtAl/somatic_mutation_data/ALL/ALL_clean_somatic_mutations_for_signature_analysis.txt;
#my $mutation_list_file = "20131111_ALL_mut_list.txt";
my $scriptFullPath = `readlink -f $0`;
my $scriptFolder = $scriptFullPath;
$scriptFolder =~ s/[^\/]+$//;
my $knowngene_file = $scriptFolder . "annotations/knownGene_complete.txt";


my $proximal_intron_dist = 300;
my $splice_site_dist	 = 2; # splice site 
my $extension 			 = 1000;
my $verbose 			 = 0;
my $outputfile;
my $vcf_input			 = 0;

my @ARGV0 = @ARGV;

GetOptions(
	"a:s"	=>\$knowngene_file,
	"o:s"	=>\$outputfile,
	"e:i"	=>\$extension,
	"s:i"	=>\$splice_site_dist,
	"p:i"	=>\$proximal_intron_dist,	
	"v"		=>\$verbose,
	'vcf-input' => \$vcf_input,
	'chrCol:i' => \$chr_col,
	'posnCol:i' => \$start_posn_col
);

if (@ARGV != 1){
	print "Take in list of positions and assess which part of a gene the position localizes to\n";
	print "Usage: $prog [options] <mutation_file.txt> \n";
	print " -a  [file]          : Annotation file to use (default=$knowngene_file)\n";
	print " -o  [file]          : File to output results into (default: output to stdout)\n";
	print " -e  [int]           : Nucleotides to extend to define as upstream/downstream of a gene (default=$extension)\n";
	print " -s  [int]           : Nucleotides from splice junction into intron to define as splice site (default=$splice_site_dist)\n";
	print " -p  [int]           : Nucleotides from splice junction to define as proximal intron (default=$proximal_intron_dist)\n";
	print " --vcf-input         : Input file is in the vcf-format\n";
	print "\n";
	print "Formatting of input File\n";
	print " -chrCol  [int]      : Column where the chromosome column is (0-based)\n";
	print " -posnCol [int]      : Column where the position column is (0-based)\n";
	print " --chrom_full_format\n";
	print " --input-from-stdin  : Take in input from STDIN (non-func)\n";
	print " -v                  : verbose (non-func)\n";	
	
	exit (0);
}


if ($vcf_input){
	$chr_format_full 	= 0; #vcf format indicate chr in 1,2,3,...
	$chr_col 			= 0;
	$start_posn_col 	= 1;
}



my ($mutation_file) = @ARGV;


&main($mutation_file);

sub main{
	# Defines the region in which a somatic mutation lies within (i.e. 5'UTR, 3'UTR etc.)
	
	my $file 		= $_[0];
	#my $output_file = $_[1];
	
	
	my %knowngene_HOAOA;
	
	# Pass by copy. Not a good idea b/c it wastes ram and is messy but then I'm 
	# too lazy to recode everything as a reference rather than a hash!! :P
	%knowngene_HOAOA = %{ &load_ucsc_knowngene() };

	# Assuming, that variant comes in the following format
	# @variant -> ("chromosome", "position", "reference_nt", "mutant_nt")
	# my %variant = (chr => "chr1", posn => "2008740", ref => "A", alt => "G");
	
	
	open (my $input, "<", $file) || die $!;
	
	my %variant_stats = (
		'5UTR' 				=> 0,
		'CDS'				=> 0,
		'3UTR'				=> 0,
		'intron_proximal'	=> 0,
		'intron_distal'		=> 0,
		'intron_splice'		=> 0,
		'upstream'			=> 0,
		'downstream'		=> 0,
		'intergenic'		=> 0,
		'ncRNA'				=> 0
	);
	
	
	# Print header for output
	#my @output_header = ('chromosome', 'position', 'Patient_id', 'ref_allele', 'mut_allele', 'gene_symbol', 'location', 'fwd_seq', 'rev_seq', 'fwd_seq_score_array', 'rev_seq_score_array', 'fwd_seq_mut_score_array', 'rev_seq_mut_score_array');
	#print join ("\t", @output_header) . "\n";
	
	
	while(<$input>){
		my $mutation_info_line = $_;
		$mutation_info_line =~ s/\r\n//;
		chomp($mutation_info_line);
		my @line_array = split(/\t/, $mutation_info_line);
		
		# Add "chr" to the chromosome num if it does not contain it
		unless($chr_format_full){
			$line_array[$chr_col] = "chr" . $line_array[$chr_col];
		}
		
		my $chr 		= $line_array[$chr_col];
		my $posn 		= $line_array[$start_posn_col]; # posn is 1-based (same as UCSC browser)
		
	
		# Generate hash where different annotations are stored
		my %gene_list;
		my %transcript_list;
		my %transcript_location;
		my %anno_hash = (
			'5UTR' 				=> 0,
			'CDS'				=> 0,
			'3UTR'				=> 0,
			'intron_proximal'	=> 0,
			'intron_distal'		=> 0,
			'intron_splice'		=> 0,
			'upstream'			=> 0,
			'downstream'		=> 0,
			'intergenic'		=> 0,
			'ncRNA'				=> 0,
			'error'				=> 0,
			'gene_symbol'		=> \%gene_list,
			'transcripts'		=> \%transcript_list,
			'transcript_location' => \%transcript_location
		);
		
		
		# Select the chromosome AOA based on the variant chromosome
		my $chr_AOA = $knowngene_HOAOA{ $chr };
		
		# Loop through all annotations for this chromosome
		foreach (@{ $chr_AOA } ){
			my $transcription_start	= ${ $_ }[3] + 1; # coord is zero-based start
			my $transcription_end	= ${ $_ }[4];	  # coord is one-based end
			
			# Check if it overlaps with the annotation
			# Add $extension to left and right so we can check if it is upstream/downstream gene variant
			if (&point_range_overlap($posn, $transcription_start - $extension, $transcription_end + $extension) ){
				# Identify region in which the variant is found
				%anno_hash = %{ &point_gene_region($_ , $posn, \%anno_hash) };
			}
			else{
				$anno_hash{'intergenic'} += 1;
			}
		}
		
		
		# Make final call for what region the variant lies in.
		my @priority = ('CDS', '5UTR', '3UTR', 'intron_splice', 'intron_proximal', 'intron_distal', 'upstream', 'downstream', 'ncRNA', 'intergenic', 'error');
		#my @priority = ('3UTR', 'CDS', '5UTR', 'intron_splice', 'intron_proximal', 'intron_distal', 'upstream', 'downstream', 'ncRNA', 'intergenic', 'error');


		# List out all the variants of a specific class
		PRIORITY: foreach my $annotation (@priority){
			if ( $anno_hash{$annotation} >= 1 ){
				
				# If intergenic is the priority, then annotate transcript location as NA
				if ($annotation eq "intergenic"){
					my %intergenic_empty_hash = ("NA"=>1);
					$anno_hash{transcript_location} = \%intergenic_empty_hash;
				}
				
				$variant_stats{$annotation} += 1;
				print join ("\t", @line_array) . "\t";
				print join("\|",keys(%{ $anno_hash{gene_symbol} })) . "\t" . $annotation . "\t";
				# Print transcript ID together with the region it is in
				print join("\|",keys(%{ $anno_hash{transcript_location} })) . "\n";

				last PRIORITY;
			}
		}
	}
	
	print STDERR $file . "\n" if $verbose;
	while(my ($k,$v) = each(%variant_stats) ){
		print STDERR $k . "\t" . $v . "\n" if $verbose;
	}
	
	
	return ($file, \%variant_stats);
	close($input);
}



#----------------------------------------------------

sub load_ucsc_knowngene{

	# Generate Knowngene HOAOA
	
	
	
	open (KNOWNGENE, $knowngene_file) || die $!;
	
	my $header = <KNOWNGENE>;
	chomp($header);
	
	# The hash storing the AOA references for each chromosome
	my %knowngene_HOAOA;
	
	# Generate an AOA for each chromosome 
	# (this makes processing later on more efficient)
	while(my $line = <KNOWNGENE>){
		chomp($line);
		my @line_array = split(/\t/, $line);
		my $chromosome = $line_array[1];
		
		# Create AOA if it does not already exist
		unless( exists($knowngene_HOAOA{$chromosome}) ){
			my @AOA;
			$knowngene_HOAOA{$chromosome} = \@AOA;
		}	
		my $AOA_ref = $knowngene_HOAOA{$chromosome};
		push (@{ $AOA_ref }, \@line_array);
	}
	
	# Loop through each chromosome (all the keys for the knowngene table)
	foreach my $chr ( keys(%knowngene_HOAOA) ){
		# Sort the AOA by the start coordinate and the end coordinate
		# Column 3 -> transcription start
		# Column 4 -> transcription stop
		@{ $knowngene_HOAOA{$chr} } = sort { $a->[3] <=> $b->[3] || $a->[4] <=> $b->[4] } @{ $knowngene_HOAOA{$chr} };
	}
	
	# #print the AOA
	# foreach (@ {$knowngene_HOAOA{chr1} }){
		# print join("\t", @{ $_ }) . "\n";
		
	# }

	close(KNOWNGENE);
	
	return \%knowngene_HOAOA;
}



#----------------------------------------------------

sub load_gencode_gene{



}





#----------------------------------------------------


sub point_gene_region{
	# Identify which part of a gene the mutation lies in 
	
	my $line_array 	= $_[0]; # line with all the annotations
	my $point		= $_[1];
	my $anno_hash	= $_[2];
	
	
	
	my $transcription_start = @{ $line_array }[3] + 1; #b/c zero-based start
	my $transcription_end 	= @{ $line_array }[4];
	my $CDS_start			= @{ $line_array }[5] + 1; #b/c zero-based start
	my $CDS_end				= @{ $line_array }[6];
	my $exon_start_string	= @{ $line_array }[8];
	my $exon_end_string		= @{ $line_array }[9];
	my $strand				= @{ $line_array }[2];
	my $transcript_id		= @{ $line_array }[11];
	my $gene_symbol			= @{ $line_array }[16];
	
	
	my @exon_start 	= split(/\,/, $exon_start_string);
	my @exon_end	= split(/\,/, $exon_end_string);
	
	# Convert exon start coords from 0-based to 1-based
	for(my $i=0; $i<scalar(@exon_start); $i++){
		$exon_start[$i] += 1
	}
	
	
	# Check if variant lies within transcriptional unit
	if ( &point_range_overlap ($point, $transcription_start, $transcription_end) ){
		${ $anno_hash }{'gene_symbol'} -> {$gene_symbol} = 1;
		${ $anno_hash }{'transcripts'} -> {$transcript_id} = 1;
		
		# Check if annotation is for a non-coding RNA
		# b/c UCSC is really weird in that even though it is zero-based start and 1-based end,
		# it will still display start and end coord of CDS for ncRNA as the same
		# Since I converted start posn to 1-based, I need to reconvert back now.
		if($CDS_start - 1 == $CDS_end){ 
			# annotate as non-coding RNA
			${ $anno_hash }{'ncRNA'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'ncRNA' . ";" . $strand} += 1;
		}
		# Annotation is for protein coding gene. Now need to identify which part
		else{
			# Modify the $anno_hash array with new annotations discovered
			$anno_hash = &define_exon_intron_anno($anno_hash, $point, \@exon_start, \@exon_end, $CDS_start, $CDS_end, $strand, $transcript_id);
		}
	}
	
	# Check if variant is upstream of the transcription end site
	elsif ( &point_range_overlap($point, $transcription_start - $extension, $transcription_start - 1) ){
		# Check strand
		if ($strand eq "+"){
			# Annotate as upstream of 5'UTR
			${ $anno_hash }{'upstream'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'upstream' . ";" . $strand} += 1;
		}
		else{
			# This is downstream of 3'UTR (negative strand)
			${ $anno_hash }{'downstream'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'downstream' . ";" . $strand} += 1;
		}
	}
	# Check if variant is downstream of the transcription end site
	elsif ( &point_range_overlap($point, $transcription_end + 1, $transcription_end + $extension) ){
		# Annotate as downstream of 3'UTR
		# Check strand
		if ($strand eq "+"){
			# This is downstream of 3'UTR (negative strand)
			${ $anno_hash }{'downstream'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'downstream' . ";" . $strand} += 1;
		}
		else{
			# Annotate as upstream of 5'UTR
			${ $anno_hash }{'upstream'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'upstream' . ";" . $strand} += 1;
		}
	}
	else{
		# Can't be intergenic as range does not allow. Annotate as error
		${ $anno_hash }{'error'} += 1;
		$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'error' . ";" . $strand} += 1;
		#print "error"; # should note have this case
	}
	
	# Add the gene symbol and transcript ID to this hash
	$anno_hash->{gene_symbol}->{$gene_symbol} += 1;
	$anno_hash->{transcripts}->{$transcript_id} += 1;
	
	
	return $anno_hash;
}






#&define_exon_intron_anno(\%anno_hash);

sub define_exon_intron_anno{
	# Defines which region of a gene a position lies in 
	# (i.e. 5UTR, 3UTR, CDS of an exon or splice_site, proximal_intron, distal_intron of an intron)
	my $anno_hash 		= $_[0];
	my $point 			= $_[1];
	my $exon_start_array 	= $_[2];
	my $exon_end_array 		= $_[3];
	my $CDS_start 		= $_[4];
	my $CDS_end 		= $_[5];
	my $strand			= $_[6];
	my $transcript_id 	= $_[7];
	
	# my $point = 20001;
	# my $exon_start_array 	= [1000,20000,40000];
	# my $exon_end_array 		= [2000,30000,50000];
	# my $CDS_start = 21100;
	# my $CDS_end = 22000;
	# my $strand = "-";

	my $proximal_intron_dist = 300;
	my $splice_site_dist	 = 2; # splice site 

	
	# if it is in here, then it is an exon (i.e. check if variant lies in exon. If so, raise exon_flag)
	my $exon_flag = 0;
	foreach (my $i=0; $i< scalar(@{ $exon_start_array }); $i++){
		my $exon_start = @{ $exon_start_array }[$i];
		my $exon_end   = @{ $exon_end_array }[$i];
		
		if ( &point_range_overlap($point, $exon_start, $exon_end) ){
			# indicate that point is in exon
			#print "exon";
			$exon_flag = 1;
		}
	}

	# If variant lies within exon, then we'll do the following
	# Check which part of an exon it is in (e.g. 5UTR, 3UTR, CDS)
	if ($exon_flag){
		# Check if it is 
		if ($strand eq "+" && $point < $CDS_start){
			# Is in 5'UTR
			#print "5UTR";
			${ $anno_hash }{'5UTR'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . '5UTR' . ";" . $strand} += 1;
		}
		elsif ($strand eq "-" && $point < $CDS_start){
			# Is in 3'UTR
			#print "3UTR";
			${ $anno_hash }{'3UTR'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . '3UTR' . ";" . $strand} += 1;
		}
		elsif ($strand eq "+" && $point > $CDS_end){
			# Is in 3'UTR
			#print "3UTR";
			${ $anno_hash }{'3UTR'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . '3UTR' . ";" . $strand} += 1;
		}
		elsif ($strand eq "-" && $point > $CDS_end){
			# Is in 5'UTR
			#print "5UTR";
			${ $anno_hash }{'5UTR'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . '5UTR' . ";" . $strand} += 1;
		}
		else{
			# Is in CDS
			#print "CDS";
			${ $anno_hash }{'CDS'} += 1;
			$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'CDS' . ";" . $strand} += 1;
		}
	}
	# otherwise, it will be in intron
	# Check which part of an intron it is in
	else{
		foreach (my $i =0; $i< scalar(@{ $exon_start_array }) - 1; $i++){ #b/c there is one less intron than exons
			my $intron_start = @{ $exon_end_array 	}[$i] + 1; #end of exon plus 1
			my $intron_end	 = @{ $exon_start_array }[$i + 1] - 1; #start of next exon minus 1
			
			# Check if it is in this intron
			if ( &point_range_overlap($point, $intron_start, $intron_end) ){
				# Check if it is in proximal intron
				if(	&point_range_overlap ($point, $intron_start, $intron_start + $proximal_intron_dist - 1) or 
					&point_range_overlap ($point, $intron_end - $proximal_intron_dist + 1, $intron_end	 ) ){
					
					# Check if it is in splice site
					if ( &point_range_overlap ($point, $intron_start, $intron_start + $splice_site_dist - 1) or 
						 &point_range_overlap ($point, $intron_end - $splice_site_dist + 1, $intron_end	 ) ){
						# Annotate it is in intron splice site
						#print "intron splice site";
						${ $anno_hash }{'intron_splice'} += 1;
						$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'intron_splice' . ";" . $strand} += 1;
					}
					else{
						# Annotate that it is in proximal intron
						#print "proximal intron";
						${ $anno_hash }{'intron_proximal'} += 1;
						$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'intron_proximal' . ";" . $strand} += 1;
					}
				}
				# Else it is in distal intron
				# You could have a case where the intron is very short and nts in intron is proximal intron
				# Thus, it is important to do it this way by exclusion
				else{
					# Annotate that it is in distal intron
					#print "distal intron";
					${ $anno_hash }{'intron_distal'} += 1;
					$anno_hash -> {transcript_location} -> {$transcript_id . ';' . 'intron_distal' . ";" . $strand} += 1;
				}
			}
		}
	}
	
	return $anno_hash;
}

#--------------------------------------------------------

	









###########################
##### Helper Function #####
###########################

#--------------------------------------------------------

sub point_range_overlap{
# Test if a point lies within a range or not
# Input:
# Point, range start and range end
#
# Output:
# True or false
	my $point		= $_[0];
	my $range_start	= $_[1];
	my $range_end	= $_[2];
	
	if ($point <= $range_end && $point >= $range_start){
		return 1; # 1 for true
	}
	else{
		return 0; # 0 for false
	}
}




#-----------------------------------------------




