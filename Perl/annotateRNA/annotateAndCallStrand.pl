#!/usr/bin/perl

use strict;
use warnings;

my $file = $ARGV[0];

if(@ARGV != 1){
	print "Usage: $0 <file>\n";
	exit(0);
}


my $scriptFullPath = `readlink -f $0`;
my $scriptFolder = $scriptFullPath;
$scriptFolder =~ s/[^\/]+$//;

my $annotateRNA = $scriptFolder . "annotateRNAFeaturesPipe.pl";
my $callStrand = $scriptFolder . "callStrandAndNucConversion.pl";

system("$annotateRNA -chrCol 0 -posnCol 1 $file > $file.annotated");
system("$callStrand $file.annotated > $file.annotated.strand");
