#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use File::Basename; 
use Parallel::ForkManager;
use File::Path qw(make_path);


# Get path of prog and scripts
my $prog = basename($0);



my $novoalignProg = "/home/kartong/software/hpc_software/novoalign/novoalign/novocraftV3.01.02.Linux3.0/novoalign";
my $novoalignGenome = "/home/kartong/software/hpc_software/novoalign/hg19_refgenome/hg19.nix";



my $timestamp = `date +%s_%N`;
chomp($timestamp);

my $tmpFolder = "./tmpNovoAlign" . $timestamp . "/";
my $label = "testing";
my $threadsCount = 10;
my $outputNovoNativeFormat = 0;
my $noSoftClip = 0;

GetOptions(
        "p=s" => \$novoalignProg,
        "g=s" => \$novoalignGenome,
        "t=i" => \$threadsCount,
        "F=s" => \$tmpFolder,
        "nativeFormat" => \$outputNovoNativeFormat,
	"noSoftClip" => \$noSoftClip,
        "l=s" => \$label
);


my $fastq = $ARGV[0];
my $outputFile = $ARGV[1];

if ( @ARGV != 2 ){
        print "Performs parallel alignment of fastq file using novoalign\n";
        print "Usage: $prog [options] <fastqFile> <outputFile>\n";
        print " ---------------------------------------------------------------------------\n";
        print "  -p [str]       : Novoalign Program Path          (default = $novoalignProg )\n";
        print "  -g [str]       : Genome to perform alignment on  (default = $novoalignGenome)\n";
        print "  -t [int]       : Number of threads to use        (default = $threadsCount)\n";
        print "  -F [str]       : Temporary folder to use         (default = $tmpFolder)\n";
        print "  -l [str]       : Label for temp file             (default = $label)\n";
        print "  --nativeFormat : Generate output as novoalign Native format output\n";
	print "  --noSoftClip   : Disable softclipping (default for Native output format)\n";
        exit (0);
}


make_path($tmpFolder);
novoalign_parallel($fastq, $threadsCount, $outputFile);
system("rm -rf $tmpFolder");


sub novoalign_parallel{
# Split the fastq file into multiple small chunks
# seperately align each of these small chunks of fastq files with multithreading
# reassemble the output into a single file
# Input:
	my $fastq_file		= $_[0];
	my $threads 		= $_[1];
	my $SubOutputFile	= $_[2];

	my $pm = new Parallel::ForkManager($threads);

	my @wordcount_array = split(/\s/, `wc -l $fastq_file`);
	my $fastq_linecount = $wordcount_array[0];

	# Split the fastq files into the desired number of chunks for parallel processing
	# Fastq file has to be in multiples of 4 lines
	my $quotient = int($fastq_linecount / ($threads * 4 ));
	my $line_per_file = (int($quotient) + 1) * 4;


	my $splitPrefix = $tmpFolder . $label;
	my $CMD = "split -l $line_per_file $fastq_file $splitPrefix";
	print $CMD . "\n";
	system($CMD);


	my @file_list = split(/\n/, `find $splitPrefix*`);


	my @fileOutputList;
	foreach my $fastq_chunk (@file_list){
		print $fastq_chunk . "\n";
		my $outputTmpNovo = $fastq_chunk . '.novo';
		my $outputTmpBam = $fastq_chunk . '.bam';
		my $outputTmpBamSorted = $fastq_chunk . '.sorted.bam';
		my $outputTmpErr = $fastq_chunk . '.err';
		
		if($outputNovoNativeFormat){
			push (@fileOutputList, $outputTmpNovo);
		}
		else{
			push (@fileOutputList, $outputTmpBamSorted);	
		}


		$pm->start and next; # do the fork
		# Clip library alignment
		if($outputNovoNativeFormat){			
			system("$novoalignProg -t 85 -d $novoalignGenome -f $fastq_chunk -l 25 -s 1 -o Native -r None 2> $outputTmpErr 1> $outputTmpNovo");
		}
		else{
			if($noSoftClip){
				system("$novoalignProg -t 85 -d $novoalignGenome -f $fastq_chunk -l 25 -s 1 -o SAM -o FullNW -r None 2> $outputTmpErr | samtools view -bS - > $outputTmpBam");
			}
			else{
				system("$novoalignProg -t 85 -d $novoalignGenome -f $fastq_chunk -l 25 -s 1 -o SAM -r None 2> $outputTmpErr | samtools view -bS - > $outputTmpBam");
			}
			system("samtools sort -o $outputTmpBam $outputTmpBamSorted > $outputTmpBamSorted");
			system("rm $outputTmpBam");
		}
		$pm->finish;
	}
	$pm->wait_all_children;



	if($outputNovoNativeFormat){
		system("cat $fileOutputList[0] > $SubOutputFile");
		for(my $i=1;$i<scalar(@fileOutputList);$i++){
			`grep "^[^#]" $fileOutputList[$i] >> $SubOutputFile`;
		}
	}
	else{
		my $bamString = join(" ", @fileOutputList);
		system("samtools merge $SubOutputFile $bamString");
		system("samtools index $SubOutputFile");
	}
	


	# Remove all the chunks of fastqfiles
	foreach my $fastq_chunk (@file_list){
		system("rm $fastq_chunk");
	}

	# Remove all the small chunks of novoalign/BAM files
	foreach my $novoChunk (@fileOutputList){
		#system("rm $novoChunk");
	}

}
