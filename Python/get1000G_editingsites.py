import sys
sys.path.insert(1,'/home/kartong/Code/functions/python/NGS/')
from mpileup import pileup
import re
import os
import subprocess
import multiprocessing as mp
import commands
#import itertools
import random
import re

ramdisk = '/media/kt/ramdisk/tmp/'
#hg19 = '/media/kt/ramdisk/TCGA_genome/GRCh37-lite.fa'

#hg19 = '/speed/kartong/KT_package/hg19_kt/hg19.fa'
hg19 = '/media/kt/ramdisk/hg19.fa'

#find /media/kt/tcga_1/all_data/ -name "*.bam"


# Parse variants from list
# Copy particular file to ramdisk
# Do counts for positions of interest
# Combine results


class varInfo:
	'''
	Object with the information of the variant of
	interest. Allows it to return required information
	easily when method is called.
	'''
	def __init__(self, posnInfo):
		self.chr 	= posnInfo[0]
		self.posn 	= posnInfo[1]
		#self.end 	= posnInfo[2]
		self.ref 	= posnInfo[3].upper()
		self.alt 	= posnInfo[4].upper()

	def Chrom(self):
		return self.chr

	def Posn(self):
		return self.posn

	def RefNt(self):
		return self.ref

	def AltNt(self):
		return self.alt


def readPosns(file):
	'''
	Reads the variant position file of interest out
	and convert it into a list of objects.
	'''
	list = []
	with open(file, 'r') as f:
		for line in f:
			varObj = varInfo(line.strip().split('\t'))
			list.append(varObj)
	return list


def copyFiles(bamfile, destination):
	'''
	Copy both the bam file and the bam index file 
	to the desired destination
	'''
	#cmd = 'cp' + bamfile + destination
	#cmd = 'cp bamfile' + destination
	baifile = bamfile + '.bai'
	cmd = ' '.join(['cp', bamfile, destination])
	cmd2 = ' '.join(['cp', baifile, destination])
	print cmd
	print cmd2
	subprocess.check_output(cmd, shell=True)
	subprocess.check_output(cmd2, shell=True)










def extractBamPosnInfo(bamfile, chrom, posn):
	'''
	Performs the pileup on the location and single bamfile 
	of interest. Gives back the counts and VAF.
	'''
	refNt 		= "NA"
	varnt 		= "NA"
	ntCountStr 	= "0,0,0,0,0"
	depth 		= "0"
	VAF 		= "NA"


	# Run the command and read it as a stream
	region = chrom + ':' + posn + '-' + posn
	cmd = 'samtools mpileup -Bf %s %s -r %s' %(hg19, bamfile, region)

	#FNULL = open(os.devnull, 'w')
	#stream = subprocess.Popen(cmd, shell=True, stderr=FNULL, stdout=subprocess.PIPE).stdout
	#os.system(cmd)
	#print os.popen(cmd)

	#print commands.getoutput(cmd).split("\n")
	f = os.popen(cmd, 'r')

	#for line in f:
		#print str(line.split("\n")) + "shit"
	lineslist = f.readline().strip().split("\n")
	#print lineslist


	# Clean up the stream result and parse as a list
	# lineslist = stream.read().strip().split("\n")
	lineslist = filter(None, lineslist) # Filter out empty elements in list


	# Only continue here if list is not empty
	#print chrom
	#print posn
	#print cmd
	if lineslist:
		arrLine = lineslist[0].split("\t")
		pileupstr = arrLine[4]

		refNt = arrLine[2]
		a = pileup(refNt, pileupstr)
		#print pileupstr

		varnt = a.VarNT()
		ntCountStr = ','.join(map(str, [a.NTcounts()['A'], a.NTcounts()['T'], a.NTcounts()['G'], a.NTcounts()['C'], a.NTcounts()['N']]))
		depth = a.depth()
		VAF = a.VAF()

	#print "shit"

	result = [chrom, posn, refNt, varnt, ntCountStr, str(depth), str(VAF)]
	return result






class pileupPosnInfo:
	'''
	An object to describe the pileup list for a single
	position for single sample. This allows easy retrieval 
	of the information of interest with the corresponding
	method.
	'''

	def __init__(self, mpileupPosnList):
		self.chr 		= mpileupPosnList[0]
		self.posn 		= mpileupPosnList[1]
		self.refnt 		= mpileupPosnList[2]
		self.depth 		= mpileupPosnList[3]
		self.pileupstr 	= mpileupPosnList[4]
		self.qualstr 	= mpileupPosnList[5]



	def Chrom(self):
		return self.chr

	def Posn(self):
		return self.posn

	def RefNt(self):
		return self.refnt

	def Depth(self):
		return self.depth

	def pileupStr(self):
		return self.pileupstr

	def qualStr(self):
		return self.qualstr




def extractBamPileupChr(bamfile, chrom, posnDict):
	'''
	Performs the pileup on the chromosome of interest and
	only analyze the positions in the chromosome which occurs
	in a given list of interest
	'''
	
	# Run the command and read it as a stream
	#region = chrom
	region = 'chr' + str(chrom)
	#region = 'chr' + str(chrom) + ':9825337-9825437'
	#region = 'chr' + str(chrom) + ':9825438-9825438'
	#region = 'chr' + str(chrom) + ':9825437-9825437'
	cmd = 'samtools mpileup -Bf %s %s -r %s' %(hg19, bamfile, region)
	print cmd
	pileupStream = os.popen(cmd, 'r')


	shit = []
	for line in pileupStream:
		
		lineList = line.strip().split("\t")
		


		try:
			pileupPosn = pileupPosnInfo(lineList)
		except:
			# In some cases there are only 4 columns as that posn sits 
			# in the middle and have no depth and no pileup string
			continue

		#chrcurr = str( pileupPosn.Chrom()[3:] )
		#print pileupPosn.Chrom()
		#chrcurr = str( pileupPosn.Chrom()[3:] )
		chrcurr = pileupPosn.Chrom()[3:]
		#chrcurr = pileupPosn.Chrom().sub("^chr", '')
		#chrcurr = re.sub("^chr", '', pileupPosn.Chrom())
		#print chrcurr + "shit"
		#print type(chrcurr)

		
		key = chrcurr + '_' + pileupPosn.Posn()
		#print key
		
		
		if key in posnDict:
			# Some default variables
			refNt 		= "NA"
			varnt 		= "NA"
			ntCountStr 	= "0,0,0,0,0"
			depth 		= "0"
			varNtCount	= "0"
			VAF 		= "NA"
			


			a = pileup(pileupPosn.RefNt(), pileupPosn.pileupStr())


			# Arrange and display the results we need
			varnt 		= a.VarNT()
			varNtCount 	= a.varNtCount()
			ntCountStr 	= ','.join(map(str, [a.NTcounts()['A'], a.NTcounts()['T'], a.NTcounts()['G'], a.NTcounts()['C'], a.NTcounts()['N']]))
			depth 		= a.depth()
			VAF 		= a.VAF()

			if depth == 0:
				continue

			result = [chrcurr, pileupPosn.Posn(), pileupPosn.RefNt(), varnt, ntCountStr, str(depth), varNtCount, str(VAF)]
			shit.append(result)

	return shit




def extractBamPileupChrParallel((bamfile, chrom, posnDict)):
	return extractBamPileupChr(bamfile, chrom, posnDict)



def singleFileTasks(filename):
	destination = ramdisk
	copyFiles(filename, destination)
	label = filename.strip().split('/')[-1]
	
	tmpfile = ramdisk + label
	

	# Perform task on file and save output

	outfolder = "/speed/kartong/testingfolder/"

	outfile = outfolder + label
	out = open(outfile, 'w')



	region = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'X', \
	'11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', 'Y', 'M']
	#region = ['20', '21', '22', 'Y', 'M']
	#region = ['21']
	#region = ['M']



	p = mp.Pool(12)


	# Generate the list of list used to feed in arguments into the parallel prog
	parallelInput = []
	for chrom in region:
		parallelInput.append([tmpfile, chrom, posnDict])

	#print posnDict

	results = p.map(extractBamPileupChrParallel, parallelInput)

	#print results


	for chrResults in results:
		for posn in chrResults:
			#print '\t'.join(str(x) for x in posn)
			out.write('\t'.join(str(x) for x in posn) + "\n")

	out.close()

	os.system("rm %s" %(tmpfile))
	os.system("rm %s%s" %(tmpfile, ".bai"))



#####################################################
#####################################################







#####################################################
#####################################################


if __name__ == '__main__':
#def main(file):
	#posnslist = readPosns("20150407_allPossibleEditings.txt")
	posnslist = readPosns("/media/kt/ramdisk/GEUVADIS_combined.snp.txt")

	

	file = sys.argv[1]

	posnDict = {}

	for data in posnslist:
		chrom = re.sub('chr', '', data.Chrom())
		posnLab = '_'.join([chrom, data.Posn()])
		posnDict[posnLab] = 1


	# 6 files and 8 cores each


	# with open('/speed/kartong/Polly/mutationReversal/1000genome_rnaseq/inputfiles/1000gRNAseqFiles.clean.txt.csv', 'r') as f:
	# 	files = f.read().splitlines()

	# # Randomize order of files so we will get files from hd1 and hd2 almost equally
	# #random.seed(123456)
	# #random.shuffle(files)

	# #print '\n'.join(files)

	# # Define number of files to process at a time
	# filePool = mp.Pool(processes=2)


	# results = filePool.map(singleFileTasks, files)
	singleFileTasks(file)
	

