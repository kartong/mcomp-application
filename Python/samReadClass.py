

class samRead:
	'''
	An object type to represent a SAM format read
	'''
	
	def __init__(self, samLine):
		samLineArr = samLine.split("\t")
		self.readName	= samLineArr[0]
		self.Flag	= samLineArr[1]
		self.Chr	= samLineArr[2]
		self.Posn 	= samLineArr[3]
		self.mapQ	= samLineArr[4]
		self.Cigar	= samLineArr[5]
		self.Seq 	= samLineArr[9]
		self.QualVals	= samLineArr[10]
		#print self.readPosn 

	def getNucleotideAtPosn(self, reqrPositionRelativeChr):
		'''
		Get the nucleotide of the base at the position
		'''
		posnRelativeRead = int(float(reqrPositionRelativeChr)) - int(float(self.Posn))
		if posnRelativeRead < 0:
			return '-'
		elif posnRelativeRead > len(self.Seq) - 1:
			return '-'
		else:
			return self.Seq[posnRelativeRead:posnRelativeRead+1]
		



#apple = samRead(3, 'abcdefghij')
#print apple.getNucleotideAtPosn(130)
